//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("TvMaze.Client, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "C:\\repos\\TVMaze\\src\\TvMaze.Client", "TvMaze.Client.csproj", "0")]
[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("TvMazeScrapperDP.Api, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "C:\\repos\\TVMaze\\src\\TvMazeScrapperDP.Api", "TvMazeScrapperDP.Api.csproj", "0")]
[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("TvMazeScrapperDp.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "C:\\repos\\TVMaze\\src\\TvMazeScrapperDp.Core", "TvMazeScrapperDp.Core.csproj", "0")]
[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("TvMazeScrapperDP.Persistance.MongoDb, Version=1.0.0.0, Culture=neutral, PublicKey" +
    "Token=null", "C:\\repos\\TVMaze\\src\\TvMazeScrapperDP.Persistance.MongoDb", "TvMazeScrapperDP.Persistance.MongoDb.csproj", "0")]
[assembly: System.Reflection.AssemblyCompanyAttribute("TvMazeScrapperDP.Api.Tests")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Debug")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("1.0.0.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("1.0.0")]
[assembly: System.Reflection.AssemblyProductAttribute("TvMazeScrapperDP.Api.Tests")]
[assembly: System.Reflection.AssemblyTitleAttribute("TvMazeScrapperDP.Api.Tests")]
[assembly: System.Reflection.AssemblyVersionAttribute("1.0.0.0")]

// Generated by the MSBuild WriteCodeFragment class.

